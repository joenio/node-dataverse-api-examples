const dataverse = require('js-dataverse');

const client = new dataverse.DataverseClient('https://demo.dataverse.org/');

/**
 * Search all kinds of content from Dataverse API search endpoint.
 * @param {string} q - The terms to be used on the search, see Dataverse API doc for details.
 * @param {string} type - The kind of the itens being searched (eg: dataset, file, etc).
 * @param {integer} start - The positioning item to start retrieves, refers API pagination.
 * @param {function} [onItem = (item) => console.log(item.name)] - A callback function to be executed on each result item.
 * @see https://guides.dataverse.org/en/latest/api/search.html
 */
async function search(q, type, start, onItem = (item) => console.log(item.name)) {
  let result = await client.search({query: q, type: type, startPosition: start, itemsPerPage: 10});
  let json = result.data;
  for (let item of json.data.items) {
    onItem(item)
  }
  if (json.data.start + json.data.count_in_response < json.data.total_count) {
    search(q, type, json.data.start + json.data.count_in_response, onItem);
  }
}

/**
 * Retrieve all public dataverses from Dataverse API search endpoint.
 * @param {string} [q = *] - The terms to be used to search dataverses, see Dataverse API doc for details.
 * @param {integer} [start = 0] - The positioning item to start retrieve dataverses, refers to API pagination.
 */
function dataverses(q = '*', start = 0) {
  search(q, 'dataverse', start, (item) => console.log(`dataverse: ${item.name}`));
}

/**
 * Retrieve all public datasets from Dataverse API search endpoint.
 * @param {string} [q = *] - The terms to be used to search datasets, see Dataverse API doc for details.
 * @param {integer} [start = 0] - The positioning item to start retrieve datasets, refers to API pagination.
 */
function datasets(q = '*', start = 0) {
  search(q, 'dataset', start, (item) => console.log(`dataset: ${item.name}`));
}

/**
 * Retrieve all public files from Dataverse API search endpoint.
 * @param {string} [q = *] - The terms to be used to search files, see Dataverse API doc for details.
 * @param {integer} [start = 0] - The positioning item to start retrieve files, refers to API pagination.
 */
function files(q = '*', start = 0) {
  search(q, 'file', start, (item) => console.log(`file: ${item.name}`));
}

dataverses();
datasets();
files();
