# Nodejs examples using Dataverse API

## `node-fetch` example

The `node-fetch-example/` dir contains an example consuming Dataverse API using
the [`node-fetch`][node-fetch] NPM module.

This example searchs for all `dataverses`, `datasets` and `files` on the
[demo.dataverse.org][demo-dataverse] instance and prints it's names on the
command line output.

How to run:

    cd node-fetch-example/
    node index.js

See the [node-fetch-example JSDoc][node-fetch-example] for details.

## `js-dataverse` example

The `js-dataverse-example/` dir contains an example consuming Dataverse API
using the [`js-dataverse`][js-dataverse] NPM module.

This example do exactly the same as `node-fetch-example`, except  for the fact
that it use `js-dataverse` instead of `node-fetch`.

See the [js-dataverse-example JSDoc][js-dataverse-example] for details.

[node-fetch]: https://www.npmjs.com/package/node-fetch
[js-dataverse]: https://www.npmjs.com/package/js-dataverse
[demo-dataverse]: https://demo.dataverse.org
[node-fetch-example]: https://joenio.gitlab.io/node-dataverse-api-examples/node-fetch-example
[js-dataverse-example]: https://joenio.gitlab.io/node-dataverse-api-examples/js-dataverse-example
