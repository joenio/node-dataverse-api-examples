const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.render('index.njk', { name: 'James' });
});

router.get('/hello', (req, res) => {
  res.send('World!');
});

module.exports = router;
