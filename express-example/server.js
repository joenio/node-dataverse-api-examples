const app = require('./app');
const port = 3000;

app.listen(port, () => {
  console.log(`express-example app listening at http://localhost:${port}`);
});
