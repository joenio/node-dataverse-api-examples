const express = require('express');
const nunjucks = require('nunjucks');
const router = require('./routes');
const app = express();

// serve static files (CSS, JS and Images)
app.use(express.static('public'));

// using nunjucks templating system
nunjucks.configure('views', {
  autoescape: true,
  express: app
});

// plug routes into express app
app.use('/', router);

module.exports = app;
